def log(operation_function):
    
    def wrapped_operation(*args):
        result = operation_function(*args)
        print(f"{operation_function.__name__}{args} = {result}")
        return result
    
    return wrapped_operation
